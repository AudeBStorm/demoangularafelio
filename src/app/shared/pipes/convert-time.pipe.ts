import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'convertTime'
})
export class ConvertTimePipe implements PipeTransform {

  //Quand on crée le pipe, on l'ajoute dans les déclarations + exports de SharedModule
  //On ajoute ensuite le SharedModule dans les imports du ExerciceModule
  transform(value: number): string {
    let minutes : number = Math.floor(value / 60); 
    let secondes : number = value % 60;
    //Rappelle ternaire : (condition)? si oui : si non
    return `${(minutes < 10) ? '0'+minutes : minutes}:${(secondes < 10) ? '0'+secondes : secondes}`;  
  }

}
