import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DemoComponent } from './demo/demo.component';
import { Demo1Component } from './demo/demo1/demo1.component';
import { ExerciceComponent } from './exercice/exercice.component';
import { HomeComponent } from './home/home.component';
import { NotfoundComponent } from './notfound/notfound.component';

const routes: Routes = [
  { path: "", component : HomeComponent },
  { path: "home", component : HomeComponent },
  // { path: "demo", component : DemoComponent, children : [
  //   { path : "demo1", component : Demo1Component },
  // ]}
  { path : "demo", component : DemoComponent, 
      loadChildren : () => import("./demo/demo.module").then(m => m.DemoModule)},
  { path : "exercice", component : ExerciceComponent, 
      loadChildren : () => import("./exercice/exercice.module").then(m => m.ExerciceModule)},
  { path : "notfound", component : NotfoundComponent },
  { path : "**", redirectTo : "notfound" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
