import { Component, OnInit } from '@angular/core';
import { Genderize } from 'src/app/models/genderize';
import { GenderizeService } from 'src/app/services/genderize.service';

@Component({
  selector: 'app-demo8',
  templateUrl: './demo8.component.html',
  styleUrls: ['./demo8.component.scss']
})
export class Demo8Component implements OnInit {

  prenom : string = "";
  codesPays : any[] = [
    { code : "BE", pays : "Belgique"},
    { code : "FR", pays : "France"},
    { code : "IT", pays : "Italie"},
    { code : "US", pays : "Etats-Unis"},
    { code : "MA", pays : "Maroc"}
  ];
  codePays : string = "BE";

  reponseApi! : Genderize;

  constructor(private _genderService : GenderizeService) { }

  ngOnInit(): void {
  }

  search(){
    this._genderService.getGender(this.prenom, this.codePays).subscribe({
      next : (res) => {
        console.log(res);
        this.reponseApi = res;
      },
      error : () => {},
      complete : () => {}
    })
  }
}
