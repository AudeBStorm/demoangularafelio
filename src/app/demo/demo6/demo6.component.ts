import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';

@Component({
  selector: 'app-demo6',
  templateUrl: './demo6.component.html',
  styleUrls: ['./demo6.component.scss']
})
export class Demo6Component implements OnInit {

  //On a besoin d'une propriété de type FormGroup pour stocker notre formulaire
  registerForm : FormGroup;
  listJobs : string[] = ["Chomeur", "Sans Emploi", "Développeur", "Cuisinier", "Chauffeur poids-lourd", "Personnel d'entretien", "Autre"];

  //On doit injecter FormBuilder, qui va nous permettre de créer notre formulaire
  constructor(private _builder : FormBuilder) {
    //On crée un nouveau formulaire grâce à notre FormBuilder et on le stocke dans notre propriété registerForm
    this.registerForm = this._builder.group({
      lastname : [null, [Validators.required, Validators.minLength(2), Validators.maxLength(100)]],
      firstname : [null, [Validators.required, Validators.minLength(2), Validators.maxLength(100)]],
      email : [null, [Validators.required, Validators.email]],
      password : [null, [Validators.required]],
      birthdate : [null, [Validators.required, this.majeurValidator()]],
      height : [null, [Validators.min(50), Validators.max(250)]],
      married : [false, []],
      job : ["", [Validators.required]]
    })
   }

  ngOnInit(): void {
    
  }

  register(){
    if(this.registerForm.invalid){
      //Si l'entièreté du formulaire est invalide, on va marquer tous les champos comme touchés
      this.registerForm.markAllAsTouched();
    }
    else {
      console.log(this.registerForm.value);
      //Construit un object json avec toutes les valeurs du formulaire
      console.log(new Date(this.registerForm.value.birthdate));

      //A faire normalement avec une db :
      //let user = new User(...this.registerForm.value); //Crée l'objet à insérer en db
      //this._authService.register(user);
       
    }    
  }

  //Custom validator
  majeurValidator() : ValidatorFn | null {
    //On reçoit le control sur lequel le validateur est testé
    return (control : AbstractControl) => {
      //control.value -> //'1989-10-16'
      if(control.value) {
        //On va fabriquer une date pour plus de facilité de manipulation
        let dateATest : Date = new Date(control.value);
        //On teste si la personne est majeure
        if(new Date().getFullYear() - dateATest.getFullYear() < 18){
          //Si < 18 on renvoie une erreur
          return { mineur : true }
        }
      }
      return null;
    }
  }

}
