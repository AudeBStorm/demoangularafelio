import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo2',
  templateUrl: './demo2.component.html',
  styleUrls: ['./demo2.component.scss']
})
export class Demo2Component implements OnInit {

  maChaine : string = "Je suis une chaine de caractères";
  monNombre : number = 15.4578965748;
  maDate : Date = new Date();

  maTemperature : number = 10;

  tempEnC : number = 25.9;
  tempEnF : number = 74.8;

  constructor() { }

  ngOnInit(): void {
  }

}
