import { Component, OnInit } from '@angular/core';
import { IFormateur } from 'src/app/models/formateur';

@Component({
  selector: 'app-demo3',
  templateUrl: './demo3.component.html',
  styleUrls: ['./demo3.component.scss']
})
export class Demo3Component implements OnInit {

  //Pour le ngModel
  favoriteFood : string = "";

  //Pour le ngStyle
  rage : string = "green";

  //Pour le ngClass
  isBig : boolean = false;
  isBold : boolean = false;
  isItalic : boolean = false;

  //Pour le ngIf
  isMale : boolean = true;

  //Pour le ngSwitch
  jourSemaine : string = "";

  //Pour le ngFor
  //simple
  listeCourse : string[] = ["Poulet", "Patates", "Bananes", "Champignons"];
  //avec objet
  formateurs : IFormateur[] = [
    { id : 1, lastname : "Chaineux", firstname : "Gavin", gender : "m", birthdate : new Date(1993, 9, 18), avatar : "../../assets/images/gavin.png"},
    { id : 2, lastname : "Ly", firstname : "Khun", gender : "m", birthdate : new Date(1982, 4, 3), avatar : "../../assets/images/khun.png"},
    { id : 3, lastname : "Beurive", firstname : "Aude", gender : "f", birthdate : new Date(1989, 9, 16), avatar : "../../assets/images/aude.png"}
  ];

  constructor() { }

  ngOnInit(): void {
  }

  switchGros() : void {
    this.isBig = !this.isBig; //On inverse le boolean
  }

  switchGras() : void {
    this.isBold = !this.isBold;
  }

  switchItalic() : void {
    this.isItalic = !this.isItalic;
  }

  //ngIf
  switchGender(gender : string) : void {
    if(gender == "male"){
      this.isMale = true;
    }
    else {
      this.isMale = false;
    }
  }

}
