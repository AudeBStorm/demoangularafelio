import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { FakeauthService } from 'src/app/services/fakeauth.service';

@Component({
  selector: 'app-demo5',
  templateUrl: './demo5.component.html',
  styleUrls: ['./demo5.component.scss']
})
export class Demo5Component implements OnInit {

  connectedUser : undefined | User;
  login : string = "";
  password : string = "";

  constructor(private _authService : FakeauthService) { }

  ngOnInit(): void {
    this._authService.connectedUser$.subscribe({
      next : (res) => {
        console.log("Demo5 a reçu User (ou undefined) : ", res);
        this.connectedUser = res;
      },
      error : () => {},
      complete : () => {}
    })
    //Si vous ne fournissez qu'une seule méthode et pas un objet, c'est toujours le next
    //this._authService.connectedUser$.subscribe((res) => { console.log(res) })
  }

  connect() : void {
    if(this.login.trim() != "" && this.password.trim() != "")
    //Avant
    //this.connectedUser = this._authService.login(this.login, this.password);
    //Après
    this._authService.login(this.login, this.password);
  }

  disconnect() : void {
    //Avant
    //this.connectedUser = this._authService.logout();
    //Après
    this._authService.logout();
  }
}
