import { Component, OnInit } from '@angular/core';
import { Formateur } from 'src/app/models/formateur';
import { FakecontactService } from 'src/app/services/fakecontact.service';

@Component({
  selector: 'app-demo7',
  templateUrl: './demo7.component.html',
  styleUrls: ['./demo7.component.scss']
})
export class Demo7Component implements OnInit {

  mesContacts : Formateur[] = [];

  constructor(private _contactService : FakecontactService) { }

  ngOnInit(): void {
    //On peut récupérer la liste de tous nos contacts grâce au service et la stocker dans notre prop mesContacts
    this.mesContacts = this._contactService.getAll();
  }

}
