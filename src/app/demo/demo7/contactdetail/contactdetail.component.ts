import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Formateur } from 'src/app/models/formateur';
import { FakecontactService } from 'src/app/services/fakecontact.service';

@Component({
  selector: 'app-contactdetail',
  templateUrl: './contactdetail.component.html',
  styleUrls: ['./contactdetail.component.scss']
})
export class ContactdetailComponent implements OnInit {

  contact : Formateur | undefined;
  id : number = 0;

  //ActivatedRoute : Vous permet de récupérer des infos sur la route active
  //Router : Vous permet de récupérer des infos sur le routing du projet et d'accéder à des méthodes pour naviguer etc
  constructor(private _routeActive : ActivatedRoute, 
              private _contactService : FakecontactService,
              private _router : Router) { }

  ngOnInit(): void {
    //Pour récupérer la valeur d'un paramètre :
    this.id = parseInt(this._routeActive.snapshot.params['id']);
    this.contact = this._contactService.getById(this.id);
    //Si on n'a pas réussi à récupérer de contact :
    if(!this.contact) {
      //Pour forcer la navigation vers la page notfound si id pas trouvé en "db"
      this._router.navigateByUrl("/notfound");
    }
  }

}
