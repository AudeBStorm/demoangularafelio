import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Demo4childComponent } from './demo4child.component';

describe('Demo4childComponent', () => {
  let component: Demo4childComponent;
  let fixture: ComponentFixture<Demo4childComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Demo4childComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Demo4childComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
