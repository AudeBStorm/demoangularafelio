import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo4',
  templateUrl: './demo4.component.html',
  styleUrls: ['./demo4.component.scss']
})
export class Demo4Component implements OnInit {

  monNom : string = "Robert";
  message : string = "En attente d'une réaction de mon enfant";

  constructor() { }

  ngOnInit(): void {
  }

  repondreBesoinEnfant(besoin : string){
    switch(besoin) {
      case 'bonjour' : 
        this.message = "Oui bonjour fiston";
        break;
      case 'manger' :
        this.message = "Mange ta main et garde l'autre pour demain";
        break;
      case 'jouer' :
        this.message = "Tu fais le goal";
        break;
      default : 
        this.message = "Fils, je ne comprends pas un mot";
        break;
    }
  }
}
