export class User {
    id : number;
    firstname : string;
    login : string;
    password : string;

    constructor(id : number, firstname : string, login : string, password : string){
        this.id = id;
        this.firstname = firstname;
        this.login = login;
        this.password = password;
    }
}