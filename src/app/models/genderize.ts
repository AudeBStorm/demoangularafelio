export class Genderize {
    count :	number;
    country_id : string;
    gender : string ;
    name : string;
    probability : number ;

    constructor(count : number, countryid : string, gender : string, name : string, proba : number) {
        this.count = count;
        this.country_id = countryid;
        this.gender = gender;
        this.name = name;
        this.probability = proba;
    }
}