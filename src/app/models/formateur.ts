//Deux façons : Soit interface, on indique toutes les propriétés que doit respecter notre objet
export interface IFormateur {
    id : number;
    lastname : string;
    firstname : string;
    gender : string;
    birthdate : Date;
    avatar : string;
}


//Si objet complexe, on peut plutôt passer par une classe avec constructeur, méthodes etc...
export class Formateur {
    id : number;
    lastname : string;
    firstname : string;
    gender : string;
    birthdate : Date;
    avatar : string;

    constructor(id : number, lastname : string, firstname : string, gender : string, birthdate : Date, avatar : string){
        this.id = id;
        this.lastname = lastname;
        this.firstname = firstname;
        this.gender = gender;
        this.birthdate = birthdate;
        this.avatar = avatar;
    }
}