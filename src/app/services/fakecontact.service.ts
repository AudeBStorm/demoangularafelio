import { Injectable } from '@angular/core';
import { Formateur } from '../models/formateur';

@Injectable({
  providedIn: 'root'
})
export class FakecontactService {

  private _contacts : Formateur[] = [
    new Formateur(15, "Beurive", "Aude", "female", new Date(1989,9,16), "../../assets/images/aude.png"),
    new Formateur(56, "Chaineux", "Gavin", "male", new Date(1993,9,18), "../../assets/images/gavin.png")
   ]

  constructor() { }

  getAll() : Formateur[] {
    return this._contacts;
  }

  getById(id : number) : Formateur | undefined {
    return this._contacts.find(f => f.id === id);
  }
}
