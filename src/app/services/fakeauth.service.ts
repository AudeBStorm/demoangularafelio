import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class FakeauthService {

  //ng g s services/fakeauth -> Pour générer un service
  //ng generate service services/fakeauth -> Pour générer un service

  //liste d'utilisateurs (à remplacer à par un accès db)
  private _users : User[] = [
    { id : 1, firstname : 'Khun', login : 'khunly', password : 'test1234'},
    { id : 2, firstname : 'Aude', login : 'audebe', password : 'test5678' }
  ];

  //Avant les observables
  // private _connectedUser : undefined | User;
  //Avec observables

  //Le Subject se déclenche quand une valeur est émise (next, error)
  //private _connectedUser$ : Subject<User | undefined> = new Subject<User | undefined>();
  //Le Behavior se déclenche quand une valeur est émise, quand on se subscribe (et donc au lancement de l'application il se déclenche déjà une première fois)
  private _connectedUser$ : BehaviorSubject<User | undefined> = new BehaviorSubject<User | undefined>(undefined);
  connectedUser$ = this._connectedUser$.asObservable();

  constructor() { }

  //Avant obversable
  // getConnectedUser() : User | undefined {
  //   return this._connectedUser;
  // }

  //Avant Observable
  // login(login : string, password : string) : undefined | User {
  //   //Trouver dans la fakedb, l'utilisateur qui a le login entré en paramètre
  //   let foundUser = this._users.find(u => u.login == login);
  //   if(foundUser){ //Si user avec bon login trouvé, on teste le password
  //     if(foundUser.password == password){
  //       this._connectedUser = foundUser; //On place l'utilisateur trouvé dans l'utilisateur connecté
  //     }
  //   }

  //   return this._connectedUser; //Si pas trouvé en fakedb -> undefined, si trouvé -> User
  // }

  //Après
  login(login : string, password : string) : void {
    //Trouver dans la fakedb, l'utilisateur qui a le login entré en paramètre
    let foundUser = this._users.find(u => u.login == login);
    if(foundUser){ //Si user avec bon login trouvé, on teste le password
      if(foundUser.password == password){
        this._connectedUser$.next(foundUser);
      }
    }
  }

  //Avant
  // logout() : undefined {
  //   //On efface l'utilisateur connecté
  //     this._connectedUser = undefined;
  //     return this._connectedUser;
  // }

  //Après
  logout() : void {
    //On efface l'utilisateur connecté
    this._connectedUser$.next(undefined);
  }
}
