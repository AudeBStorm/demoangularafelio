import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Genderize } from '../models/genderize';

@Injectable({
  providedIn: 'root'
})
export class GenderizeService {

  private _url : string = "https://api.genderize.io/";

  constructor(private _httpClient : HttpClient) { }

  getGender(prenom : string, codePays : string) : Observable<Genderize> {
    return this._httpClient.get<Genderize>(`${this._url}?name=${prenom}&country_id=${codePays}`);
  }
}
