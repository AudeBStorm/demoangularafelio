import { TestBed } from '@angular/core/testing';

import { FakecontactService } from './fakecontact.service';

describe('FakecontactService', () => {
  let service: FakecontactService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FakecontactService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
