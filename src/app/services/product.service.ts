import { Injectable } from '@angular/core';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private _products: Product[] = [
    { id: 1, name: 'Patate', quantity: 3 },
    { id: 6, name: 'Poulet', quantity: 1 }
  ]

  constructor() { }

  getAll() : Product[] {
    return this._products;
  }

  add(produit : Product) : void { 
      //On teste si le produit est déjà dans notre liste
      let foundProduct = this._products.find(product => product.name.toLowerCase() == produit.name.toLowerCase().trim());
      if(foundProduct){
        //Si un produit a été trouvé, on augmente la quantité de 1
        foundProduct.quantity++;
      }
      else { //Si le produit n'est pas trouvé, on l'ajoute
        //On trouve l'id max dans le tableau pour créer un prduit avec l'idmax auquel on ajoute 1
        let idMax = Math.max(...this._products.map(product => product.id));
        produit.id = idMax + 1;
        this._products.push(produit);
      }
  }

  delete(id : number) {
    this._products.splice(this._products.findIndex(p => p.id == id), 1);
  }

  upQty(id : number) {
    let foundProduct = this._products.find(p => p.id == id);
    if(foundProduct){
      foundProduct.quantity++;
    }
  }

  downQty(id : number) {
    let foundProduct = this._products.find(p => p.id == id);
    if(foundProduct){
      foundProduct.quantity--;
      if(foundProduct.quantity == 0) {
        this.delete(id);
      }
    }
  }
}
