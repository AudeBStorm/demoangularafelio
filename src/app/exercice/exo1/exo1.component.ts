import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-exo1',
  templateUrl: './exo1.component.html',
  styleUrls: ['./exo1.component.scss']
})
export class Exo1Component implements OnInit {

  totalSecondes : number = 0;
  timer : any = undefined;

  constructor() { }

  ngOnInit(): void {
  }

  play() : void {
    this.timer = setInterval( () => { this.totalSecondes ++} , 1000);
  }

  pause() : void {
    clearInterval(this.timer); //arrête le timer
    this.timer = undefined;
  }

  reset() : void {
    //Remise à 0 du nombre de secondes
    this.totalSecondes = 0;

    //+ arrêt du timer
    this.pause();
  }

}
