import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExerciceRoutingModule } from './exercice-routing.module';
import { ExerciceComponent } from './exercice.component';
import { Exo1Component } from './exo1/exo1.component';
import { SharedModule } from '../shared/shared.module';
import { Exo2Component } from './exo2/exo2.component';
import { FormsModule } from '@angular/forms';
import { ShowListComponent } from './exo2/show-list/show-list.component';


@NgModule({
  declarations: [
    ExerciceComponent,
    Exo1Component,
    Exo2Component,
    ShowListComponent
  ],
  imports: [
    CommonModule,
    ExerciceRoutingModule,
    FormsModule,
    SharedModule
  ],
  bootstrap : [
    ExerciceComponent
  ]
})
export class ExerciceModule { }
