import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Product } from 'src/app/models/product';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-show-list',
  templateUrl: './show-list.component.html',
  styleUrls: ['./show-list.component.scss']
})
export class ShowListComponent implements OnInit {

  //Avant service
  //@Input() listToShow : Product[] = [];
  //@Output() onDelete : EventEmitter<number>;
  //@Output() onUpQt : EventEmitter<number>;
  //@Output() onDownQt : EventEmitter<number>;

  listToShow : Product[] = [];

  constructor(private _productService : ProductService) {
    //this.onDelete = new EventEmitter<number>();
    //this.onUpQt = new EventEmitter<number>();
    //this.onDownQt = new EventEmitter<number>();
  }

  ngOnInit(): void {
    this.listToShow = this._productService.getAll();
    //A linitialisation du componant, on va chercher la liste des produits et on la stocke dans notre propriété listToShow
  }

  //Avant service
  // sendId(id : number){
  //   this.onDelete.emit(id); //On déclenche l'évènement onDelete et on envoie l'id via l'event
  // }

  //Après service, plus besoin d'event
  delete(id : number) {
    this._productService.delete(id); //On appelle le service qui s'occupe de supprimer
  }

  //Avant service : On doit emettre l'event au parent qui va s'occuper d'augmenter la quantité de 1
  // onUp(id : number){
  //   this.onUpQt.emit(id); //On déclenche l'évènement onUpQt et on lui envoie l'id
  // }

  //Après service : 
  upQt(id : number) : void {
    this._productService.upQty(id); //On appelle la méthode dans le service qui s'occupe de faire +1 à la qty
  }

  //Avant service : On doit emettre l'event au parent qui va s'occuper de diminuer la quantité de 1
  // onDown(id : number){
  //   this.onDownQt.emit(id); //On déclenche l'évènement onDownQt et on lui envoie l'id
  // }

  //Après service
  downQt(id : number) : void {
    this._productService.downQty(id); //On appelle la méthode dans le service qui s'occupe de faire -1 à la qty
  }
}
