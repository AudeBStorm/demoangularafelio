import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/product';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-exo2',
  templateUrl: './exo2.component.html',
  styleUrls: ['./exo2.component.scss']
})
export class Exo2Component implements OnInit {

  //Exo avant service
  // products: Product[] = [
  //   { id: 1, name: 'Patate', quantity: 3 },
  //   { id: 6, name: 'Poulet', quantity: 1 }
  // ]

  productToAdd: string = "";

  constructor(private _productService : ProductService) { }

  ngOnInit(): void {
  }

  // addProduct() {
  //   //Si l'utilisateur n'a rentré aucun caractère dans l'input, on ne créera pas de nouvel objet
  //   if (this.productToAdd.trim() != '') {
  //     //On teste si le produit est déjà dans notre liste
  //     let foundProduct = this.products.find(product => product.name.toLowerCase() == this.productToAdd.toLowerCase().trim());
  //     if(foundProduct){
  //       //Si un produit a été trouvé, on augmente la quantité de 1
  //       foundProduct.quantity++;
  //     }
  //     else { //Si le produit n'est pas trouvé, on l'ajoute
  //       //On trouve l'id max dans le tableau pour créer un prduit avec l'idmax auquel on ajoute 1
  //       let idMax = Math.max(...this.products.map(product => product.id));
  //       this.products.push(new Product(idMax + 1, this.productToAdd.trim(), 1));
  //     }

  //     this.productToAdd = "";
  //   }
  // }

  addProduct(){
      this._productService.add(new Product(0, this.productToAdd.trim(), 1));
      this.productToAdd = "";
  }

  // deleteProduct(id : number){
  //   //si id est un id
  //   //On supprime à partir de l'index de l'élément dont l'id est égal à celui reçu en paramètre et on n'en supprime qu'un
  //   this.products.splice(this.products.findIndex(product => product.id == id), 1);

  //   //si id est un index
  //   // this.products.splice(id, 1); //On supprime à partir de l'index reçu et on ne supprime qu'un seul élément
  // }

  // upQt(id : number){
  //   //Trouver l'élément qui possède cet id et faire l'update de la qtt
  //   let foundProduct = this.products.find(product => product.id == id);
  //   //Si on trouve le produit
  //   if(foundProduct){
  //     //+1 à la quantité
  //     foundProduct.quantity++;
  //   }

  // }

  // downQt(id : number){
  //   //Trouver l'élément qui possède cet id et faire l'update de la qtt
  //   let foundProduct = this.products.find(product => product.id == id);
  //   if(foundProduct){
  //     foundProduct.quantity--;
  //     //Si après le -, a quantité arrive à 0, on le supprime de la liste
  //     if(foundProduct.quantity == 0)
  //     {
  //       this.products.splice(this.products.findIndex(p => p.id == foundProduct?.id), 1);
  //     }
  //   }

  // }

}
