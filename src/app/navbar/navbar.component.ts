import { Component, OnInit } from '@angular/core';
import { Link } from '../models/link';
import { User } from '../models/user';
import { FakeauthService } from '../services/fakeauth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  links : Link[] = [
    { title : "Accueil", url : "/home" },
    { title : "Demos", url : "/demo", children : [
      { title : "Demo 1 - Les Bindings", url : "/demo/demo1"},
      { title : "Demo 2 - Les Pipes", url : "/demo/demo2"},
      { title : "Demo 3 - Les Directives", url : "/demo/demo3"},
      { title : "Demo 4 - Input / Output", url : "/demo/demo4"},
      { title : "Demo 5 - Services", url : "/demo/demo5"},
      { title : "Demo 6 - Les Formulaires", url : "/demo/demo6"},
      { title : "Demo 7 - Routing Dynamique", url : "/demo/demo7"},
      { title : "Demo 8 - HttpClient (Api)", url : "/demo/demo8"}
    ], isVisible : true},
    { title : "Exercices", url : "/exercice", children : [
      { title : "Exercice 1 - Le Timer", url : "/exercice/exo1"},
      { title : "Exercice 2 - La Shopping List", url : "/exercice/exo2"}
    ], isVisible : false}
  ];

  connectedUser : User | undefined;

  constructor(private _authService : FakeauthService) { 
  }

  ngOnInit(): void {
    //this.connectedUser = this._authService.getConnectedUser();
    this._authService.connectedUser$.subscribe({
      //Fonction qui traite la valeur émise par le Subject
      next : (res) => {
        console.log("Navbar a reçu User (ou undefined) : ", res);
        this.connectedUser = res; //On stocke le user (ou undefined) reçu en param
      },
      //Fonction qui traite les erreurs si y'en a
      error : (error) => {},
      //Fonction déclenchée à la fin de la vie de notre subject
      complete : () => {}
    })
  }

  toggleMenu(index : number){
    let currentState = this.links[index].isVisible;
    this.links.forEach(link => link.isVisible = false);
    this.links[index].isVisible = !currentState;
    
  }

  logout() : void {
    //Avant
    //this.connectedUser = this._authService.logout();

    //Après 
    this._authService.logout();
  }
}
